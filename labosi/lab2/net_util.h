#ifndef WRAPPERS_INCLUDED
#define WRAPPERS_INCLUDED

#include <unistd.h>

//network stuff
#include <arpa/inet.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>

//logs
#include <sys/syslog.h>
#include <stdarg.h>

/* 
 * Sets basic 'hints' addrinfo values
 * Supports family, socktype, flags
 *
 * family - AF_UNSPEC / AF_INET / AF_INET6
 * socktype - SOCK_STREAM/SOCK_DGRAM
 */
void set_hints(struct addrinfo* hints, int family, int socktype);

/*
 * Wrapper for getaddrinfo
 *
 * server - name / ip
 * service - name / port
 */
void get_addr_info(const char* server, const char* service,
		const struct addrinfo* hints, struct addrinfo** res);

void get_name_info(const struct sockaddr* addr, socklen_t addrlen,
		char* host, socklen_t hostlen, char* serv, socklen_t servlen, int flags);

/*
 * Wrapper for opening a socket
 * 
 * family - AF_INET / AF_INET6 + others(manpage)
 * type - SOCK_STREAM / SOCK_DGRAM + others(manpage)
 * protocol - default 0, manpage for details
 *
 * returns socket on success
 */
int open_socket(int family, int type, int protocol);

/*
 * Wrapper for setsockopt
 * Used for faster debugging
 */
void reuse_dead_socket(int socket);

/*
 * Wrapper for bind
 */
void bind_socket(int sockfd, const struct sockaddr* addr, 
		socklen_t addrlen);

/*
 * Wrapper for listen
 */
void sock_listen(int socket, int backlog);

/*
 * Wrapper for accept
 */
int accept_request(int sockfd, struct sockaddr* addr,
		socklen_t* addrlen);
/*
 * Wrapper za shutdown
 * SHUT_RD / SHUT_WR / SHUT_RDWR
 */
void wrap_shutdown(int sockfd, int how);

/*
 * Wrapper za close
 */
void wrap_close(int fd);

/*
 * Wrapper za connect
 */
void sock_connect(int sockfd, 
		const struct sockaddr* addr, socklen_t addrlen);

/*
 * Wrapper for read
 * Returns num of read bytes, 0 on EOF
 */
int wrap_read(int fd, char* buf, int max);

/*
 * Wrapper for write
 */
int wrap_write(int fd, char* buf, int num);

/*
 * Wrapper for send
 * Used only in connected state
 */
ssize_t wrap_send(int sockfd, const void* msg,
		size_t msglen, int flags);

/*
 * Wrapper for recv
 */
ssize_t wrap_recv(int sockfd, void* buf, size_t len, int flags);

/*
 * Wrapper for sendto
 * May be used in connectionless services
 */
ssize_t wrap_sendto(int sockfd, const void *msg, size_t len,
		int flags, const struct sockaddr* dest_addr, socklen_t tolen);

/*
 * Wrapper for recvfrom
 * Standard for raw sockets
 */
ssize_t wrap_recvfrom(int sockfd, void* buf, size_t len,
		int flags, struct sockaddr* from, socklen_t* fromlen);

#endif /* WRAPPERS_INCLUDED */