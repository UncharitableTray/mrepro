#include "net_abst.h"


void init_hints(struct addrinfo* hints, int family, int socktype) {
    memset(hints, 0, sizeof(*hints));
    hints->ai_family = family;
    hints->ai_socktype = socktype;
}


void init_tcp_client(struct addrinfo *hints, int AF_FAM)
{
    memset(hints, 0, sizeof(struct addrinfo));
    hints->ai_family = AF_FAM;
    hints->ai_flags |= AI_CANONNAME;
    syslog(LOG_DEBUG, "%s", "Initialized hints.");
}

void init_tcp_server_v4(struct sockaddr_in *my_addr, char *port)
{
    my_addr->sin_family = AF_INET;
    my_addr->sin_port = htons((unsigned short)atoi(port));
    my_addr->sin_addr.s_addr = INADDR_ANY;
    memset(my_addr->sin_zero, '\0', sizeof(my_addr->sin_zero));
}

void init_udp_server_v4(struct sockaddr_in *serveraddr, char *port)
{
    memset(serveraddr, 0, sizeof(*serveraddr));
    serveraddr->sin_family = AF_INET;
    serveraddr->sin_port = htons((unsigned short)atoi(port));
    serveraddr->sin_addr.s_addr = htonl(INADDR_ANY);
}


int get_udp_socket(char *ip, char *port, 
        struct addrinfo* hints, struct addrinfo** res)
{
    get_addr_info(ip, port, hints, res);

    if (res == NULL || *res == NULL) {
        errx(1, "Addrinfo not filled.\n");
    }

    return socket((*res)->ai_family, (*res)->ai_socktype, (*res)->ai_protocol);
}


void set_tcp_target(struct sockaddr_in *their_addr, 
        int AF_FAM, char* PORT, struct addrinfo *res)
{
	memset(their_addr, 0, sizeof(*their_addr));	
    their_addr->sin_family = AF_FAM;
    their_addr->sin_port = htons(atoi(PORT));    //pazi na port koji dolazi
    their_addr->sin_addr = ((struct sockaddr_in*)(res)->ai_addr)->sin_addr; 
    syslog(LOG_DEBUG, "%s", "Set target sockaddr_in.");
}
