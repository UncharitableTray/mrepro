#include "zad_header.h"
#include "error_util.h"
#include "net_util.h"
#include "net_abst.h"

#include <sys/poll.h>

#define OPT_LIST "t:u:p:"
#define MAX_ARGS 6
#define MIN_ARGS 0

void handle_input(char *buf, char *payload);

void handle_tcp(int sockfd, char *buf, char *payload);

void handle_udp(int sockfd, char *buf, char *payload);

int main(int argc, char **argv)
{
    int opt;
    int tcp_set = 0, udp_set = 0, payload_set = 0;
    char *TCP_PORT, *UDP_PORT;
    char *payload;

    int sock_tcp, sock_udp, sock_stdin;
    int rv;
    ssize_t ret_read;
    unsigned int ndfs = 3;
    char buf_tcp[BUFFER_SIZE], buf_udp[BUFFER_SIZE], buf_stdin[BUFFER_SIZE];
    struct pollfd ufds[3];

    struct sockaddr_in udp_serv, tcp_serv;
    
    //int index;    //if non-optional arguments are supported

    if (argc < MIN_ARGS + 1 || argc > MAX_ARGS + 1) {
        fprintf(stderr, "Usage: %s [-t tcp_port] [-u udp_port] [-p popis]\n",
            argv[0]);
        exit(EXIT_F);
    }

	payload = (char*)malloc((unsigned)sizeof(buf_tcp));

    while ((opt = getopt(argc, argv, OPT_LIST)) != -1)
    {
        switch(opt) {
            case 't':
                printf("TCP port set.\n");
                check_port_range(optarg);
                tcp_set = 1;
                TCP_PORT = optarg;
                break;
            case 'u':
                printf("UDP port set.\n");
                check_port_range(optarg);
                udp_set = 1;
                UDP_PORT = optarg;
                break;
            case 'p':
                payload_set = 1;
                payload = strncpy(payload, optarg, strlen(optarg));
				printf("Optarg je: %s, payload je: %s\n", optarg, payload);
                break;
            default:
                fprintf(stderr, "Usage: %s [-t tcp_port] [-u udp_port] [-p popis]\n",
                    argv[0]);
                exit(EXIT_F);
        }
    }
    check_too_many_params(optind, argv);

    if (!tcp_set) {
        TCP_PORT = (char*)malloc((unsigned)sizeof(DEFAULT_PORT));
        TCP_PORT = strcpy(TCP_PORT, DEFAULT_PORT);
    }
    if (!udp_set) {
        UDP_PORT = (char*)malloc((unsigned)sizeof(DEFAULT_PORT));
        UDP_PORT = strcpy(TCP_PORT, DEFAULT_PORT);
    }
    if (!payload_set) {
        payload = strcpy(payload, "");
    }

    init_tcp_server_v4(&tcp_serv, TCP_PORT);
    init_udp_server_v4(&udp_serv, UDP_PORT);

    sock_tcp = open_socket(PF_INET, SOCK_STREAM, 0);
    sock_udp = open_socket(PF_INET, SOCK_DGRAM, 0);
    sock_stdin = 0;

    reuse_dead_socket(sock_tcp);
    reuse_dead_socket(sock_udp);

    bind_socket(sock_tcp, (struct sockaddr*)(&tcp_serv), sizeof(tcp_serv));
    bind_socket(sock_udp, (struct sockaddr*)(&udp_serv), sizeof(udp_serv));

    sock_listen(sock_tcp, BACKLOG);

    ufds[0].fd = sock_stdin;
    ufds[0].events = POLLIN;

    ufds[1].fd = sock_tcp;
    ufds[1].events = POLLIN;

    ufds[2].fd = sock_udp;
    ufds[2].events = POLLIN;

    while(1)
    {
        rv = poll(ufds, ndfs, 4000);

        if (rv == -1) {
            wrap_errx("Server main", EXIT_F);
        } else if (rv == 0) {
       	//timeout occurred 
        } else {
            if (ufds[0].revents & POLLIN) {     //input poll prosao za STDIN
                ret_read = wrap_read(sock_stdin, buf_stdin, BUFFER_SIZE - 1);
                handle_input(buf_stdin, payload);
                if (ret_read <= 0) {
                    errx(EXIT_F, "Nesto poslo po zlu...");
                }
            }
            if (ufds[1].revents & POLLIN) {     //input poll prosao za TCP
                handle_tcp(sock_tcp, buf_tcp, payload);
            }
            if (ufds[2].revents & POLLIN) {     //input poll prosao za UDP
                //ret_read = wrap_read(sock_udp, buf_udp, BUFFER_SIZE - 1);
                handle_udp(sock_udp, buf_udp, payload);
            }
        }
    }

    return 0;
}

void handle_input(char *buf, char *payload)
{
    if (strncmp(buf, SERV_QUIT, strlen(SERV_QUIT) - 1) == 0) exit(0);

    if (strncmp(buf, SERV_PRINT, strlen(SERV_PRINT) - 1) == 0) {
        printf("%s\n", payload);
    }

    if (strncmp(buf, SERV_SET, strlen(SERV_SET) - 1) == 0) {
        fgets(payload, BUFFER_SIZE - 1, stdin);
        //scanf ("%[^\n]%*c", name); //mozda, jel ovo ima probleme s overflowom
		//?
		printf("Novi payload je: %s\n", payload);
    }
}

void handle_tcp(int sockfd, char *buf, char *payload)
{
    int newfd;
    struct sockaddr_in their_addr;
    ssize_t ret_read;
    socklen_t sin_size;
    char hbuf[NI_MAXHOST], sbuf[NI_MAXSERV];
    
	sin_size = sizeof(their_addr);
	newfd = accept(sockfd, (struct sockaddr*)(&their_addr), &sin_size);
	reuse_dead_socket(newfd);
	ret_read = wrap_read(newfd, buf, BUFFER_SIZE);

    if (ret_read <= 0) {
        errx(EXIT_F, "Nesto je poslo po zlu u handle_tcp");
    }

    if (strncmp(buf, HELLO, strlen(HELLO) - 1) != 0) {
        printf("Poruka (TCP) nije HELLO. %s\n", buf);
        return;
    }
       
    get_name_info((struct sockaddr*)(&their_addr), sin_size,
        hbuf, sizeof(hbuf), sbuf, sizeof(sbuf), NI_NOFQDN | NI_NUMERICSERV);
    
    //Debug print
    printf("Got connection from %s:%s\n", hbuf, sbuf);
    wrap_send(newfd, payload, strlen(payload), 0);
    printf("Poslao poruku: %s.\n", payload);
	
	//shutdown(newfd, SHUT_RDWR);
}

void handle_udp(int sockfd, char *buf, char *payload)
{
    struct sockaddr_in their_addr;
    unsigned int their_len;
    struct hostent* host;
    char buffer[BUFFER_SIZE];

	their_len = sizeof(their_addr);
	wrap_recvfrom(sockfd, buffer, BUFFER_SIZE, 0,
		(struct sockaddr*)(&their_addr), &their_len);

    if (strncmp(buffer, HELLO, strlen(HELLO) - 1) != 0) {
        debug_print("Poruka (UDP) nije HELLO.");
        return;
    }

    host = gethostbyaddr((const char *)(&their_addr.sin_addr.s_addr), 
            sizeof(their_addr.sin_addr.s_addr), AF_INET);
    if (host == NULL) {
        errx(1, "Error in gethostbyaddr in handle_udp.\n");
    }

    wrap_sendto(sockfd, (const char *)payload, strlen(payload),
                0, (struct sockaddr *)(&their_addr), sizeof(their_addr));
}
