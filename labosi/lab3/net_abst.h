#ifndef NET_ABST
#define NET_ABST

#include "net_util.h"
#include "error_util.h"

/*
 * Inits the hints structure
 */
void init_hints(struct addrinfo* hints, int family, int socktype);

/*
 * Initializes addrinfo hints for use
 * in TCP clients.
 */
void init_tcp_client(struct addrinfo *hints, int AF_FAM);

/*
 * Initializes sockaddr_in structs for use
 * in TCP servers.
 */
void init_tcp_server_v4(struct sockaddr_in *my_addr, char *port);

/*
 * Initializes sockaddr_in structs for use
 * in UDP servers.
 */
void init_udp_server_v4(struct sockaddr_in *serveraddr, char *port);

/*
 * Fills in data in sockaddr_in.
 * Used before forming TCP connections.
 * 
 * Warning: Make sure atoi(PORT) returns
 * a valid number (e.g. no services).
 */
void set_tcp_target(struct sockaddr_in *their_addr, 
        int AF_FAM, char* PORT, struct addrinfo *res);


int get_udp_socket(char *ip, char *port, 
        struct addrinfo* hints, struct addrinfo** res);

#endif