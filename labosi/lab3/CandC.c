#include "zad_header.h"
#include "error_util.h"
#include "net_util.h"
#include "net_abst.h"
#include "list.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <err.h>
#include <errno.h>

#include <sys/poll.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>


struct pair {
    char ip[INET_ADDRSTRLEN];
    char port[22];
};

struct msg {
    char command;
    struct pair pairs[20];
};

/*** Globalne varijable ***/

char* ROOT;
int listenfd, clients[BACKLOG];


void handle_stdin(char* buf_stdin, struct node** bots);

void handle_http(int client_sock, struct node** bots);

void handle_udp(int sock_udp, char* buf_udp, struct node** bots);

void print_list(struct node** head);

long int hash(char* string);

void send_message_to_bots(struct msg* message, struct node** bots);


int main(int argc, char** argv)
{
    char* cnc_tcp_port;
    struct sockaddr_in tcp_serv, udp_serv;
    int sock_tcp, sock_udp, sock_stdin;
    ssize_t ret_read;
    int client_index = 0;

    char buf_udp[BUFFER_SIZE], buf_stdin[BUFFER_SIZE];
    unsigned int ndfs = 3;
    struct pollfd ufds[ndfs];
    int rv = 0;

    //liste botova i meta
    struct node *bots = 0;

    if (argc == 2) {
        cnc_tcp_port = argv[1];
    } else if (argc == 1) {
        cnc_tcp_port = DEFAULT_TCP_PORT_CNC;
    } else {
        errx(1, "Unexpected number of parameters:\nUsage: ./CandC [tcp_port]");
    }

    ROOT = getenv("PWD");

    init_tcp_server_v4(&tcp_serv, cnc_tcp_port);
    init_udp_server_v4(&udp_serv, DEFAULT_UDP_PORT_CNC);

    sock_tcp = open_socket(PF_INET, SOCK_STREAM, 0);
    sock_udp = open_socket(PF_INET, SOCK_DGRAM, 0);
    sock_stdin = 0;

    reuse_dead_socket(sock_tcp);
    reuse_dead_socket(sock_udp);

    bind_socket(sock_tcp, (struct sockaddr*)(&tcp_serv), sizeof(tcp_serv));
    bind_socket(sock_udp, (struct sockaddr*)(&udp_serv), sizeof(udp_serv));

    sock_listen(sock_tcp, BACKLOG);

    ufds[0].fd = sock_stdin;
    ufds[0].events = POLLIN;

    ufds[1].fd = sock_tcp;
    ufds[1].events = POLLIN;
    
    ufds[2].fd = sock_udp;
    ufds[2].events = POLLIN;

    for (int i = 0; i < BACKLOG; i++) {
        clients[i] = -1;
    }

    while(1)
    {
        rv = poll(ufds, ndfs, 4000);

        if (rv == -1) {
            wrap_errx("Server main", EXIT_F);
        } else if (rv == 0) {
            //printf("Timeout occurred.\n");
        } else {
            if (ufds[0].revents & POLLIN) {     //input poll prosao za STDIN
                ret_read = wrap_read(sock_stdin, buf_stdin, BUFFER_SIZE - 1);
                buf_stdin[ret_read - 1] = '\0';
                if (ret_read <= 0) {
                    errx(EXIT_F, "Nesto poslo po zlu...");
                }
                handle_stdin(buf_stdin, &bots);
            }
            if (ufds[1].revents & POLLIN) {
                struct sockaddr_in their_addr;
                socklen_t addrlen = sizeof(their_addr);
                clients[client_index] = accept_request(sock_tcp, 
                        (struct sockaddr*)(&their_addr), &addrlen);
                
                if (clients[client_index] < 0) {
                    fprintf(stderr, "Error while accepting a TCP request\n");
                } else {
                    if (fork() == 0) {
                        handle_http(client_index, &bots);
                        exit(0);
                    }
                }

                while (clients[client_index] != -1) {
                    client_index = (client_index + 1) % BACKLOG;
                }
            }
            if (ufds[2].revents & POLLIN) {
                //ovdje se hendla registracija i naredivanje botovima
                handle_udp(sock_udp, buf_udp, &bots);
            }
        }
    }

    return 0;
}


void handle_http(int client_sock, struct node** bots)
{
	char msg[BUFFER_SIZE], *reqline[3], data_to_send[BUFFER_SIZE], path[BUFFER_SIZE];
	int rcvd, fd, numbytes;

	memset(msg, 0, BUFFER_SIZE);

	rcvd = wrap_recv(clients[client_sock], msg, BUFFER_SIZE, 0);

    if (rcvd == 0)    // receive socket closed
		fprintf(stderr,"Client disconnected.\n");
	else    // message received
	{
		//printf("Received tcp message; %s", msg);

		reqline[0] = strtok(msg, " \t\n");

		if (strncmp(reqline[0], HTTP_GET, strlen(HTTP_GET)) == 0)
		{
			reqline[1] = strtok (NULL, " \t");
			reqline[2] = strtok (NULL, " \t\n");
			if ((strncmp(reqline[2], HTTP_VER0, strlen(HTTP_VER0)) != 0) 
                    && (strncmp(reqline[2], HTTP_VER1, strlen(HTTP_VER1))!=0 ))
			{
				wrap_write(clients[client_sock], HTTP_ERR_400, strlen(HTTP_ERR_400));
			}
			else
			{
                char* command = (char*)calloc(1, BUFFER_SIZE);
				if (strncmp(reqline[1], "/\0", strlen("/\0") + 1) == 0) {
					reqline[1] = "/index.html";     //po defaultu otvara /index.html ako ne nade trazeno
                
                } else if (strcmp(reqline[1], HTTP_CMD_PT) == 0) {
                    strlcpy(command, CMD_PT, strlen(CMD_PT) + 1);
                    handle_stdin(command, bots);

                } else if (strcmp(reqline[1], HTTP_CMD_PTL) == 0) {
                    strlcpy(command, CMD_PTL, strlen(CMD_PTL) + 1);
                    handle_stdin(command, bots);

                } else if (strcmp(reqline[1], HTTP_CMD_PU) == 0) {
                    strlcpy(command, CMD_PU, strlen(CMD_PU) + 1);
                    handle_stdin(command, bots);

                } else if (strcmp(reqline[1], HTTP_CMD_PUL) == 0) {
                    strlcpy(command, CMD_PUL, strlen(CMD_PUL) + 1);
                    handle_stdin(command, bots);

                } else if (strcmp(reqline[1], HTTP_CMD_R) == 0) {
                    strlcpy(command, CMD_R, strlen(CMD_R) + 1);
                    handle_stdin(command, bots);

                } else if (strcmp(reqline[1], HTTP_CMD_R2) == 0) {
                    strlcpy(command, CMD_R2, strlen(CMD_R2) + 1);
                    handle_stdin(command, bots);

                } else if (strcmp(reqline[1], HTTP_CMD_STOP) == 0) {
                    strlcpy(command, CMD_S, strlen(CMD_S) + 1);
                    handle_stdin(command, bots);

                } else if (strcmp(reqline[1], HTTP_CMD_LIST) == 0) {
                    //TODO: napisi funkciju koja vraca html list
                    int i = 0;
                    struct pair* bot = (struct pair*)get(bots, i);
                    char* bot_message = (char*)calloc(1, BUFFER_SIZE);

                    bot_message = strcat(bot_message, HTTP_OK_200);
                    strcat(bot_message, "<html>\n<body>\n");
                    while (bot) {

                        strcat(bot_message, bot->ip);
                        strcat(bot_message, ":");
                        strcat(bot_message, bot->port);
                        strcat(bot_message, "<br>");

                        bot = (struct pair*)get(bots, ++i);
                    }
                    strcat(bot_message, "</html>\n</body>\n");
                    wrap_write(clients[client_sock], bot_message, strlen(bot_message));
                    free(bot_message);
                    free(command);
                    wrap_shutdown(clients[client_sock], SHUT_RDWR);         //All further send and recieve operations are DISABLED...
	                wrap_close(clients[client_sock]);
	                clients[client_sock] = -1;
                    return;

                } else if (strcmp(reqline[1], HTTP_CMD_QUIT) == 0) {
                    strlcpy(command, CMD_Q, strlen(CMD_Q) + 1);
                    handle_stdin(command, bots);
                }
                free(command);

				strcpy(path, ROOT);
				strcpy(&path[strlen(ROOT)], reqline[1]);
				//printf("file: %s\n", path);

				if ((fd = open(path, O_RDONLY))!=-1 )   //nasao file
				{
					send(clients[client_sock], HTTP_OK_200, strlen(HTTP_OK_200), 0);
					while ((numbytes = wrap_read(fd, data_to_send, BUFFER_SIZE)) > 0) {
                        wrap_write (clients[client_sock], data_to_send, numbytes);
                    }
				}
				else {
                    wrap_write(clients[client_sock], HTTP_ERR_404, strlen(HTTP_ERR_404));   //error 404
                }
			}
		}
	}

	//Closing SOCKET
	wrap_shutdown(clients[client_sock], SHUT_RDWR);         //All further send and recieve operations are DISABLED...
	wrap_close(clients[client_sock]);
	clients[client_sock] = -1;
}


void handle_stdin(char* buf_stdin, struct node** bots)
{
    struct msg* message = (struct msg*)malloc(sizeof(struct msg));
    memset(message, 0, sizeof(struct msg));

    switch (hash(buf_stdin))
    {
        case HASH_CMD_PT:
            message->command = '1';
            memcpy(message->pairs[0].ip, IP1, strlen(IP1));
            memcpy(message->pairs[0].port, PORT1, strlen(PORT1));
            send_message_to_bots(message, bots);
            break;      
        case HASH_CMD_PTL:
            message->command = '1';
            memcpy(message->pairs[0].ip, IP2, strlen(IP2));
            memcpy(message->pairs[0].port, PORT1, strlen(PORT1));
            send_message_to_bots(message, bots);
            break;      
        case HASH_CMD_PU:
            message->command = '2';
            memcpy(message->pairs[0].ip, IP1, strlen(IP1));
            memcpy(message->pairs[0].port, PORT1, strlen(PORT1));
            send_message_to_bots(message, bots);
            break;
        case HASH_CMD_PUL:
            message->command = '2';
            memcpy(message->pairs[0].ip, IP2, strlen(IP2));
            memcpy(message->pairs[0].port, PORT1, strlen(PORT1));
            send_message_to_bots(message, bots);
            break;
        case HASH_CMD_R:
            message->command = '3';
            memcpy(message->pairs[0].ip, IP2, strlen(IP2));
            memcpy(message->pairs[0].port, PORT2, strlen(PORT2));
            memcpy(message->pairs[1].ip, IP3, strlen(IP3));
            memcpy(message->pairs[1].port, PORT6, strlen(PORT6));
            send_message_to_bots(message, bots);
            break;
        case HASH_CMD_R2:
            message->command = '3';
            memcpy(message->pairs[0].ip, IP4, strlen(IP4));
            memcpy(message->pairs[0].port, PORT3, strlen(PORT3));
            memcpy(message->pairs[1].ip, IP5, strlen(IP5));
            memcpy(message->pairs[1].port, PORT4, strlen(PORT4));
            memcpy(message->pairs[2].ip, IP6, strlen(IP6));
            memcpy(message->pairs[2].port, PORT5, strlen(PORT5));
            send_message_to_bots(message, bots);
            break;
        case HASH_CMD_S:
            message->command = '4';
            send_message_to_bots(message, bots);
            break;
        case HASH_CMD_L:
            printf(" --> lista botova: \n");
            print_list(bots);
            break;
        case HASH_CMD_N:
            //strlcpy(message, MSG_CMD_N, sizeof(struct msg));
            send_message_to_bots(message, bots);
            break;
        case HASH_CMD_Q:
            message->command = '0';
            send_message_to_bots(message, bots);
            break;
        case HASH_CMD_H:
            printf("Ovdje treba ici ispis naredbi\n");
            break;
        default:
            break;
    }
    //printf("%s\n", (char*)message);
    free(message);
}


void handle_udp(int sock_udp, char* buf_udp, struct node** bots)
{
    struct sockaddr_storage their_addr;
    socklen_t addrlen = sizeof their_addr;
    ssize_t numbytes;
    char buf[BUFFER_SIZE];

    struct pair* bot = (struct pair*)malloc(sizeof(struct pair));

    numbytes = wrap_recvfrom(sock_udp, buf, BUFFER_SIZE, 0,
            (struct sockaddr*)(&their_addr), &addrlen);
    buf[numbytes] = '\0';

    if (strncmp(buf, REG, strlen(REG)) != 0) {
        fprintf(stderr, "Poruka dobivena od bota nije REG.\n");
    }

    get_name_info((struct sockaddr*)(&their_addr), addrlen,
            bot->ip, sizeof(bot->ip), bot->port, sizeof(bot->port),
            NI_NUMERICHOST | NI_NUMERICSERV);

    printf("Bot klijent %s:%s\n", bot->ip, bot->port);
    
    if (insert(bot, bots) != 0) {
        printf("Error while inserting an element into the list.\n");
    }
}

void print_list(struct node** head)
{
    int i = 0;
    struct pair* par = NULL;
    par = (struct pair*)get(head, i);
    
    while (par != NULL)
    {
        printf("%s:%s\n", par->ip, par->port);
        par = (struct pair*)get(head, ++i);
    }
}

long int hash(char* string)
{
    long int res = 0;
    int n = strlen(string);

    for (int i = 0; i < n; i++) {
        res = res * 1000 + string[i];
    }
    return res;
}

void send_message_to_bots(struct msg* message, struct node** bots)
{
    int i = 0;
    struct pair* bot = NULL;
    bot = (struct pair*)get(bots, i);

    int sockfd, numbytes;
    struct addrinfo hints, *res;

    while (bot)
    {
        memset(&hints, 0, sizeof(hints));
        hints.ai_family = AF_INET;
        hints.ai_socktype = SOCK_DGRAM;
        sockfd = get_udp_socket(bot->ip, bot->port, &hints, &res);
        numbytes = wrap_sendto(sockfd, message, sizeof(struct msg), 0,
                res->ai_addr, res->ai_addrlen);
        if (numbytes < 0) {
            fprintf(stderr, "Could not send message: %c%s%s\n", message->command,
                message->pairs[0].ip, message->pairs[0].port);
        } else {
            //debug_print("Sent message: ");
            //printf("of size %d\n", numbytes);
        }

        bot = (struct pair*)get(bots, ++i);  //get next
        
        wrap_close(sockfd);
        freeaddrinfo(res);
    }
}
