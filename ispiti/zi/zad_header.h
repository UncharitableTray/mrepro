#ifndef ZADHDR_INCLUDED
#define ZADHDR_INCLUDED

//ovi se cesto koriste
#define BUFFER_SIZE 1000
#define TARGET_COUNT 20
#define MSG_SIZE 20*INET_ADDRSTRLEN + 20*22

#define DEFAULT_PORT        "1234"
#define DEFAULT_HOST        "127.0.0.1"
#define DEFAULT_TCP_PORT_CNC    "80"
#define DEFAULT_UDP_PORT_CNC    "5555"

#define BACKLOG 5

#define CMD_ON 		"ON\n"
#define CMD_RESET 	"RESET\n"
#define CMD_OFF		"OFF\n"

#define CMD_PTL     "ptl"
#define CMD_PU      "pu"
#define CMD_PUL     "pul"
#define CMD_R       "r"
#define CMD_R2      "r2"
#define CMD_S       "s"
#define CMD_L       "l"
#define CMD_N       "n"
#define CMD_Q       "q"
#define CMD_H       "h"

#define HTTP_CMD_PT     "/bot/prog_tcp"
#define HTTP_CMD_PTL    "/bot/prog_tcp_localhost"
#define HTTP_CMD_PU     "/bot/prog_udp"

#define HTTP_CMD_R      "/bot/run"
#define HTTP_CMD_R2     "/bot/run2"
#define HTTP_CMD_STOP   "/bot/stop"
#define HTTP_CMD_LIST   "/bot/list"
#define HTTP_CMD_QUIT   "/bot/quit"

#define IP1         "10.0.0.20" //za PT
#define IP2         "127.0.0.1" //za PTL
#define IP3         "localhost"
#define IP4         "20.0.0.11"
#define IP5         "20.0.0.12"
#define IP6         "20.0.0.13"
#define IP_NEP      "NEPOZNATA\n"
#define PORT1       "1234"
#define PORT2       "vat"
#define PORT3       "1111"
#define PORT4       "2222"
#define PORT5       "3333"
#define PORT6       "6789"
#define MSG_CMD_H       "h"

#define HASH_CMD_PT     112116
#define HASH_CMD_PTL    112116108
#define HASH_CMD_PU     112117
#define HASH_CMD_PUL    112117108
#define HASH_CMD_R      114
#define HASH_CMD_R2     114050
#define HASH_CMD_S      115
#define HASH_CMD_L      108
#define HASH_CMD_N      110
#define HASH_CMD_Q      113
#define HASH_CMD_H      104

#define HTTP_GET        "GET\0"
#define HTTP_VER0       "HTTP/1.0"
#define HTTP_VER1       "HTTP/1.1"

#define HTTP_OK_200     "HTTP/1.0 200 OK\n\n"

#define HTTP_ERR_400    "HTTP/1.0 400 Bad Request\n"
#define HTTP_ERR_404    "HTTP/1.0 404 Not Found\n"

//ako treba ponovno racunati hash
//	printf("%ld\n", hash(CMD_PT));
//	printf("%ld\n", hash(CMD_PTL));
//	printf("%ld\n", hash(CMD_PU));
//	printf("%ld\n", hash(CMD_PUL));
//	printf("%ld\n", hash(CMD_R));
//	printf("%ld\n", hash(CMD_R2));
//	printf("%ld\n", hash(CMD_S));
//	printf("%ld\n", hash(CMD_L));
//	printf("%ld\n", hash(CMD_N));
//	printf("%ld\n", hash(CMD_Q));
//	printf("%ld\n", hash(CMD_H));
#endif
