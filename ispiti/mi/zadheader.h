#ifndef ZADHDR_INCLUDED
#define ZADHDR_INCLUDED

#define BUFFER_SIZE 512
#define DEFAULT_PORT "1234"
#define BACKLOG 10

#define MODE_ON 0
#define MODE_OFF 1
#define CONN_ON "ON"
#define CONN_OFF "OFF"

#define SEND "SEND"
#define QUIT "QUIT\n"	//ne radi ako stavim \n u definiciju
#define STAT "STAT\n"
#define OK "OK\n"

#endif

struct pair {
	char ip[INET6_ADDRSTRLEN];
	char port[6];
};

struct pair_node {
	struct pair* pair;
	struct pair_node* next;
};

int start_receiving(int sockfd);

void send_messages(int sockfd, char* message);

// mode - 0 za ON, 1 za OFF
void print_connection(struct sockaddr* peer, int mode);
