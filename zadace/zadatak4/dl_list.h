#ifndef DL_LIST_HEADER
#define DL_LIST_HEADER

#include <stdio.h>
#include <string.h>
#include <malloc.h>

struct node {
    void* data;
    struct node* next;
    struct node* prev;

};

#endif