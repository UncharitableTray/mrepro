#ifndef FILE_WRAPPERS
#define FILE_WRAPPERS

#include "error_util.h"

//file stuff
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

/* ==============================
 * Functions for file operations
=============================== */
/*
 * Wrapper for open
 * O_CREAT in flags for autocreate
 */
int file_open(const char* pathname, int flags);


/*
 * Wrapper for fopen
 */
FILE* file_fopen(const char* pathname, const char* mode);


/*
 * Wrapper for fclose
 */
void file_fclose(FILE* file);

/*
 * Wrapper for fread
 */
size_t file_fread(void* ptr, size_t size,
		size_t nmemb, FILE* stream);


/*
 * Wrapper for fwrite
 */
size_t file_fwrite(const void* ptr, size_t size,
		size_t nmemb, FILE* stream);


/*
 * Wrapper for access
 * amode = R_OK / W_OK / X_OK
 */
int file_access(const char* path, int amode);


/*
 * Checks if a file exists
 */
int file_exists(const char* path);

/*
 * Wrapper for fseek
 */
int file_fseek(FILE* stream, long offset, int whence);

/*
 * Wrapper for ftell
 */
long file_ftell(FILE* stream);

#endif