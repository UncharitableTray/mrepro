#include "zad_header.h"
#include "error_util.h"
#include "net_util.h"
#include "net_abst.h"
#include "file_util.h"

#include <time.h>

#define OPT_LIST "d"
#define MAX_ARGS 2  //3 params + 3 non-optional arguments
#define MIN_ARGS 1

int running = 1;    //za demonizaciju

void begin(char* port);

int main(int argc, char** argv) {
    int opt;
    char* port;
    
    //int index;    //if optional arguments are supported

    if (argc < MIN_ARGS + 1 || argc > MAX_ARGS + 1) {
        fprintf(stderr, "Usage: %s [-d] port_name_or_number\n",
            argv[0]);
        exit(EXIT_F);
    }

    while ((opt = getopt(argc, argv, OPT_LIST)) != -1)
    {
        switch(opt) {
            case 'd':
                printf("Pokrecem kao demon...\n");
                break;
            default:
                fprintf(stderr, "Usage: %s [-d] port_name_or_number\n",
                    argv[0]);
                exit(EXIT_F);
        }
    }

    port = argv[argc - 1];

    //if no optional arguments are supported
    if (argv[optind + 1] != NULL) {
        fprintf(stderr, "Usage: %s [-d] port_name_or_number\n",
            argv[0]);
        exit(EXIT_F);
    }

    begin(port);

    return 0;
}

void begin(char* port)
{
    time_t tim;
    FILE* log;
    struct sockaddr_in myaddr;
    int sockfd;

    srand((unsigned) time(&tim));   //init time za TID
    log = file_fopen("./log.log", "a+");
    fprintf(log, "Initialized log for tftpserver.\n");

    init_udp_server_v4(&myaddr, port);  //za slusanje dolaska na socketu
    fprintf(log, "Initialized sockaddr_in structure for port %s.\n", port);

    sockfd = open_socket(AF_INET, SOCK_DGRAM, 0);
    fprintf(log, "Opened socket %d for port %s.\n", sockfd, port);

    reuse_dead_socket(sockfd);
    fprintf(log, "Enabled reusing of socket %d.\n", sockfd);

    bind_socket(sockfd, (struct sockaddr*)(&myaddr), sizeof(myaddr));
    fprintf(log, "Bound socket %d to myaddr.\n", sockfd);

    while(running)
    {
        
    }
}
