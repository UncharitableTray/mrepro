#include "file_util.h"

/* ==============================
 * Functions for file operations
=============================== */

int file_open(const char* pathname, int flags)
{
	int res;
	if ((res = open(pathname, flags)) < 0) {
		wrap_errx("open", errno);
	}
	return res;
}


FILE* file_fopen(const char* pathname, const char* mode)
{
	FILE* res;
	if ((res = fopen(pathname, mode)) == NULL) {
		wrap_errx("fopen", errno);
	}
	return res;
}


void file_fclose(FILE* file)
{
	if (fclose(file) != 0) {
		wrap_errx("fclose", errno);
	}
}


size_t file_fread(void* ptr, size_t size,
		size_t nmemb, FILE* stream)
{
	size_t chunk;
	if ((chunk = fread(ptr, size, nmemb, stream)) != nmemb) {
		if (ferror(stream)) {
			errx(EXIT_F, "Error while reading the file.");
		}
	}
	return chunk;
}


size_t file_fwrite(const void* ptr, size_t size,
		size_t nmemb, FILE* stream)
{
	size_t chunk;
	if ((chunk = fwrite(ptr, size, nmemb, stream)) != nmemb) {
		if (ferror(stream)) {
			errx(EXIT_F, "Error while writing to the file.");
		}
	}
	return chunk;
}


int file_access(const char* path, int amode)
{
	int result;
	if ((result = access(path, amode)) == -1) {
		wrap_errx("access", errno);
	}
	return result;
}


int file_exists(const char* path)
{
	if (access(path, F_OK) != -1) {
		return 1;
	}
	return 0;
}


int file_fseek(FILE* stream, long offset, int whence)
{
	int res;
	if ((res = fseek(stream, offset, whence)) == -1) {
		wrap_errx("fseek", errno);
	}
	return res;
}


long file_ftell(FILE* stream) {
	long res;
	if ((res = ftell(stream)) == -1) {
		wrap_errx("ftell", errno);
	}
	return res;
}
