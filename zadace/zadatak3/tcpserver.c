#include "wrappers.h"
#include "zadheader.h"

#define BACKLOG 10

int check_file_status(const char* filename);

int main(int argc, char** argv) 
{
	int port, port_set = 0, opt;
	int sockfd;
	struct addrinfo hints, *res;
	char* port_string= "1234";
	socklen_t client_size;

	while((opt = getopt(argc, argv, "p:")) != -1)
	{
		switch(opt) {
			case 'p':
				port_set = 1;
				port = atoi(optarg);
				port_string = optarg;
				if (port < 1024 || port > 65535) {
						fprintf(stderr, "Usage: ./tcpserver [-p port]\n");
						exit(3);
				}
				break;
			default:
				errx(3, "Usage: ./tcpserver [-p port]");
		}
	}

	if (argv[optind] != NULL) {
		errx(1, "Too many arguments.");
	}

	if (!port_set) {
		//printf("Velicina default porta je %d\n", (int)sizeof(DEFAULT_PORT));
		port_string = (char*)malloc(sizeof(DEFAULT_PORT));
		port_string = strncpy(port_string, DEFAULT_PORT, sizeof(DEFAULT_PORT));
	}

	memset(&hints, 0, sizeof(hints));
	set_hints(&hints, AF_INET, SOCK_STREAM);
	hints.ai_flags = AI_PASSIVE;	/* Za auto popunjavanje IP adrese */
									/* OK je za servere valjda */
	get_addr_info(NULL, port_string, &hints, &res);

	sockfd = open_socket(AF_INET, SOCK_STREAM, 0);
	reuse_dead_socket(sockfd);
	bind_socket(sockfd, res->ai_addr, res->ai_addrlen);
	sock_listen(sockfd, BACKLOG);
	//printf("Uspjesno sam bindao port %s na socket %d.\n", port_string, sockfd);

	while(1)
	{
		int newsock, status, offset;
		struct sockaddr_in client;
		struct request rqst;
		char filename[BUFFER_SIZE] = {0};
		char buf[BUFFER_SIZE] = {0};
		FILE* file;

		ssize_t read;

		client_size = sizeof(client);
		
		newsock = accept_request(sockfd,(struct sockaddr*)(&client), &client_size);
		
		wrap_recv(newsock, &rqst, sizeof(rqst), 0);
		strcpy(filename, rqst.filename);
		offset = rqst.offset;
		//printf("Primio sam filename: %s.\n", filename);

		status = check_file_status(filename);
		wrap_send(newsock, &status, sizeof(status), 0);
		if (status != 0) {
			switch(status) {
				case 1:
					wrap_send(newsock, ERR_MISS, sizeof(ERR_MISS), 0);
					break;
				case 2:
					wrap_send(newsock, ERR_READ, sizeof(ERR_READ), 0);
					break;
				case 3:
					wrap_send(newsock, ERR_GNRL, sizeof(ERR_GNRL), 0);
					break;
			}
			wrap_close(newsock);
			continue;
		}

		file = file_fopen(filename, "r");
		file_fseek(file, (long)offset, SEEK_SET);
		/*
		 * Naputak za inace... zapisujem koliko sam procitao da
		 * toliko mogu tocno poslati... klijent mora pratiti
		 * koliko je dobio da tocno toliko moze i zapisati
		 * 
		 * nakon sto sam to dodao je proradilo...
		 */
		while((read = file_fread(buf, 1, BUFFER_SIZE, file)) > 0) {
			wrap_send(newsock, buf, read, 0);
			memset(buf, 0, BUFFER_SIZE);
		}

		file_fclose(file);
		wrap_close(newsock);
	}
    return 0;
}

int check_file_status(const char* filename) {
	if (file_exists(filename) == 0) return 1;
	if (file_access(filename, R_OK) == -1 ) return 2;
	if (strchr(filename, '/') != NULL) return 3;
	return 0;
}