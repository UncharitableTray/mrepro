#include "error_util.h"
#include "net_util.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <err.h>
#include <string.h>
//#include <errno.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define MAX_ARGS 1
#define MIN_ARGS 1

#define MSG             "Hello blyat\n"
#define BUFFER_SIZE     1024

int main(int argc, char** argv) {
    int sockfd;
    struct addrinfo hints, *servinfo;
    char* port;
    char buf[BUFFER_SIZE] = "\0";

    if (argc < MIN_ARGS + 1 || argc > MAX_ARGS + 1) {
        fprintf(stderr, "Usage: %s udp_port\n",
            argv[0]);
        exit(EXIT_F);
    }
    port = argv[1];
    check_port_range(port);

    //prvo punimo hints sa svim detaljima koji su potrebni za UDP
    memset(&hints, '\0', sizeof(hints));
    hints.ai_family = AF_UNSPEC;        //nebitno je li ipv4 ili ipv6
    hints.ai_socktype = SOCK_DGRAM;     //koristimo UDP, a ne TCP npr
    hints.ai_flags = AI_PASSIVE;        //automatski popuni IP adresu za hosta
    get_addr_info(NULL, port, &hints, &servinfo);   //popuni servinfo informacijama servera

    sockfd = open_socket(AF_INET, SOCK_DGRAM, 0);   //ipv4 udp with no special protocols
    reuse_dead_socket(sockfd);
    bind_socket(sockfd, servinfo->ai_addr, servinfo->ai_addrlen);   //slusaj taj socket za poruke

    freeaddrinfo(servinfo);

    int numbytes;
    struct sockaddr_in their_addr;
    socklen_t addrlen = sizeof their_addr;
    char peer_ipaddr[INET6_ADDRSTRLEN];
    while(1)
    {
        numbytes = wrap_recvfrom(sockfd, buf, BUFFER_SIZE - 1, 0,
                (struct sockaddr*)(&their_addr), &addrlen);     //server dobije poruku
        
        //napravi nesto s porukom
        printf("Listener: got pakcet from %s\n",
            inet_ntop(their_addr.sin_family,
                &their_addr.sin_addr,
                peer_ipaddr, sizeof(peer_ipaddr)));
        
        printf("Listener: packet is %d long.\n", numbytes);
        buf[numbytes] = '\0';
        printf("Listener: packet contains \"%s\".\n", buf);
        
        strlcpy(buf, MSG, strlen(MSG));
        numbytes = wrap_sendto(sockfd, buf, strlen(buf), 0,
                (struct sockaddr*)(&their_addr), addrlen);
        
        printf("Listener: sent message %s (%d bytes)\n", buf, numbytes);
    }
    return 0;
}
