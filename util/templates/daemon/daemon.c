#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>

int running = 1;    //1 znaci keep running

int main(int argc, char** argv)
{
    int pid, sid;
    FILE* log;

    pid = fork();   //forkamo i terminiramo fathera
    if (pid < 0) {
        perror("Fork");
        exit(1);
    }
    if (pid > 0) exit(0);

    umask(0);       //hitimo umask na nulu

    //otvaramo log
    log = fopen("./log.txt", "a+");
    if (log == NULL) {
        perror("fopen");
        exit(2);
    }

    //dobijemo novi process i session id
    sid = setsid();
    if (sid < 0) {
        perror("setsid");
        exit(3);
    }

    if (chdir("/") < 0) {
        perror("chdir");
        exit(4);
    }

    //zatvaramo sve streamove, odsad komuniciramo
    //samo preko log fajlova
    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    while(running)
    {
        //obavi posao
        fprintf(log, "Ja sam mala %s.\n", "vjeverica");
        running = 0;
        sleep(60);
    }

    fclose(log);
    return EXIT_SUCCESS;
}