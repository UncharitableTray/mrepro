#include "dl_list.h"
#include <stdio.h>
#include <string.h>
#include <malloc.h>

void insert(void* data, struct node** start, struct node** end)
{
    struct node* node = (struct node*)malloc((unsigned)sizeof(struct node));
    node->data = data;
    node->prev = NULL;
    
    if (!(*start)) { //first node in list
        *start = node;

        node->next = NULL;
        *end = node;
    } else {
        node->next = *start;
        *start = node;
    }
    return; 
}

void clear(struct node** start, struct node** end)
{
    struct node* ptr = *start;
    *end = NULL;
    *start = NULL;

    struct node* next;
    while (ptr) {
        next = ptr->next;
        free(ptr);
        ptr = next;
    }
    return;
}

void* get(struct node** start, int index)
{
    struct node* ret = NULL;
    struct node* ptr = *start;

    for (int i = 0; ptr; i++) {
        if (i == index) {
            return ptr->data;
        } else {
            ptr = ptr->next;
        }
    }
    return NULL;
}

// struct ip_port {
//     const char* str1;
//     const char* str2;
// };

// int main (void){
//     struct node* start;
//     struct node* end;
//     start = end = NULL;

//     struct ip_port* port1 = (struct ip_port*)malloc(sizeof(struct ip_port));
//     struct ip_port* port2 = (struct ip_port*)malloc(sizeof(struct ip_port));
//     struct ip_port* port3 = (struct ip_port*)malloc(sizeof(struct ip_port));

//     port1->str1 = "abc";
//     port1->str2 = "1";
//     port2->str1 = "abc";
//     port2->str2 = "2";
//     port3->str1 = "abc";
//     port3->str2 = "3";

//     insert(port3, &start, &end);
//     insert(port2, &start, &end);
//     insert(port1, &start, &end);

//     struct ip_port* godem1 = (struct ip_port*)get(&start, 0);
//     struct ip_port* godem2 = (struct ip_port*)get(&start, 1);
//     struct ip_port* godem3 = (struct ip_port*)get(&start, 2);

//     printf("%s\n", godem1->str2);
//     printf("%s\n", godem2->str2);
//     printf("%s\n", godem3->str2);
// }