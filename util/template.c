#include "zad_header.h"
#include "error_util.h"
#include "net_util.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define OPT_LIST "t:u:p:"
#define MAX_ARGS 6  //3 params + 3 non-optional arguments
#define MIN_ARGS 0

int main(int argc, char** argv) {
    int opt;
    
    //int index;    //if optional arguments are supported

    if (argc < MIN_ARGS + 1 || argc > MAX_ARGS + 1) {
        fprintf(stderr, "Usage: %s [-t tcp_port] [-u udp_port] [-p popis]\n",
            argv[0]);
        exit(EXIT_F);
    }

    while ((opt = getopt(argc, argv, OPT_LIST)) != -1)
    {
        switch(opt) {
            case 't':
                printf("Doso %s\n", optarg);
                break;
            case 'u':
                printf("Doso %s\n", optarg);
                //do stuff with non-optional arg
                break;
            case 'p':
                printf("Doso %s\n", optarg);
                break;
            default:
                fprintf(stderr, "Usage: %s [-t tcp_port] [-u udp_port] [-p popis]\n",
                    argv[0]);
                exit(EXIT_F);
        }
    }

    //if no optional arguments are supported
    if (argv[optind] != NULL) {
        fprintf(stderr, "Usage: %s [-t tcp_port] [-u udp_port] [-p popis]\n",
            argv[0]);
        exit(EXIT_F);
    }

    //else handle optional arguments
    //for (index = optind; index < argc; index++) {
    //    printf ("Non-option argument %s\n", argv[index]);
    //    return 0;
    //}
    return 0;
}
