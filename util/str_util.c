#include "str_util.h"
#include <string.h>
#include "list.h"
#include <stdlib.h>

struct node* string_to_list(char* str, char delim)
{
    char *frag;
    struct node* head;

    frag = strtok(str, &delim);
    while(frag != NULL) {
        char *new_str = (char*)malloc((unsigned)sizeof(frag));
        new_str = strcpy(new_str, frag);
        frag = strtok(str, &delim);

        insert((void*)new_str, &head);
    }
    return head;
}