#ifndef ZADHDR_INCLUDED
#define ZADHDR_INCLUDED

//ovi se cesto koriste
#define BUFFER_SIZE 1024
#define MSG_SIZE 20*INET_ADDRSTRLEN + 20*22

#define DEFAULT_PORT "1234"
#define DEFAULT_HOST "127.0.0.1"

#define BACKLOG 10

#define QUIT '0'
#define PROG_TCP '1'
#define PROG_UDP '2'
#define RUN '3'
#define STOP '4'

#define HELLO   "HELLO\n"
#define REG     "REG\n"

#define SERV_PRINT  "PRINT\n"
#define SERV_SET    "SET\n"
#define SERV_QUIT   "QUIT\n"

#endif